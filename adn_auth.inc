<?php

/**
 * @file
 * Authorization form for App.net.
 */

/**
 * Callback form user/%user/adn.
 */
function adn_user_auth_form($form, &$form_state) {
  $form['adn_user'] = array(
  	'#type' => 'textfield',
  	'#title' => t('App.net username'),
  	'#size' => 60,
  	'#default_value' => '',
    '#required' => TRUE,
  );
  $form['adn_password'] = array(
  	'#type' => 'password',
  	'#title' => t('App.net password'),
  	'#size' => 60,
    '#required' => TRUE,
  );
  $form['submit'] = array(
  	'#type' => 'submit',
  	'#value' => t('Authorize via App.net'),
  );
  return $form;
}

/**
 * Submit handler for adn_user_auth_form().
 */
function adn_user_auth_form_submit($form, &$form_state) {
  dpm($form);
}